# CAN Multimedia Integration #
for Audi RNS-D Navigation Plus

### Current Status ###
Identifying CAN-Ids and corresponding messages

### TODO ###
* register Arduino as TV-Modul
* playing music with VS1053-Shield
* receive CAN Messages to change songs or playback
* display song title and duration on RNS-D
* display song info in dashboard cluster
