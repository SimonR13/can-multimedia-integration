#define DEBUG_MODE 1

// Define Joystick connection pins
#define UP     A1
#define DOWN   A3
#define LEFT   A2
#define RIGHT  A5
#define CLICK  A4

//Define LED pins
#define LED2 8
#define LED3 7

// BEGIN CAN BUS
#include <SPI.h>
#include "mcp_can.h"
#include <SD.h>

// chip select ping for CAN
const int SPI_CAN_PIN = 10;
MCP_CAN CAN(SPI_CAN_PIN);

unsigned char len = 0;
unsigned char buf[32];
char str[20];
// END CAN BUS

// BEGIN uSD
// chip select pin for uSD
const int SPI_uSD_PIN = 9;
//Declare SD File
File dataFile;
// END uSD

// CAN IDs
const INT32U TV_MODULE_CAN_ID = 0x264,
             TV_MODULE_CAN_ID_2 = 0x344,
             NAV_MODULE_CAN_ID = 0x464,
             NAV_MODULE_CAN_ID_2 = 0x544,
             DATE_TIME_CAN_ID = 0x623,
             DATE_TIME_CAN_ID_2 = 0x703,
             FIN_CAN_ID = 0x65F,
             FIN_CAN_ID_2 = 0x73F,
             /* proven IDs */
             FIS_UPPER_CAN_ID = 0x341,
             FIS_LOWER_CAN_ID = 0x342;

const unsigned char TV_INIT_MSGS[14][8] = {
  { 0xE0, 0x01, 0x00, '\0', '\0', '\0', '\0', '\0' },
  { 0x10, 0x15, 0x01, 0x00, 0x02, 0x00, 0x00, '\0' },
  { 0xB1, '\0', '\0', '\0', '\0', '\0', '\0', '\0' },
  { 0x11, 0x01, 0x02, '\0', '\0', '\0', '\0', '\0' },
  { 0xB2, '\0', '\0', '\0', '\0', '\0', '\0', '\0' },
  { 0x12, 0x01, 0x01, '\0', '\0', '\0', '\0', '\0' },
  { 0xB3, '\0', '\0', '\0', '\0', '\0', '\0', '\0' },
  { 0x13, 0x09, 0x01, 0x01, 0x20, 0x01, 0x48, '\0' },
  { 0xB4, '\0', '\0', '\0', '\0', '\0', '\0', '\0' },
  { 0xB5, '\0', '\0', '\0', '\0', '\0', '\0', '\0' },
  { 0x14, 0x27, 0x01, 0x00, '\0', '\0', '\0', '\0' },
  { 0xB6, '\0', '\0', '\0', '\0', '\0', '\0', '\0' },
  { 0x15, 0x27, 0x01, 0x00, '\0', '\0', '\0', '\0' },
};

void sendInitToFis();
void sendLineToFIS(INT8U *buf, INT32U CAN_ID = FIS_UPPER_CAN_ID, INT8U msgLen = 8);

void setup() {
  //Initialize pins as necessary
  pinMode(SPI_uSD_PIN, OUTPUT);
  pinMode(CLICK,INPUT);
  pinMode(LED2, OUTPUT);
  pinMode(LED3, OUTPUT);

  //Pull analog pins high to enable reading of joystick movements
  digitalWrite(CLICK, HIGH);

  //Write LED pins low to turn them off by default
  digitalWrite(LED2, LOW);
  digitalWrite(LED3, LOW);

  Serial.begin(115200);
  delay(100);
  Serial.println("Serial initialized");

#if DEBUG_MODE
  Serial.println("DEBUG_MODE enabled!");
#endif

  for (int i=0; i<10; ++i) {
    Serial.printf("CAN init #%d\n", i);
    if (CAN_OK == CAN.begin(CAN_100KBPS)) {
      Serial.println("CAN initialized with 'CAN_100KBPS'");
      sendInitToFis();
      break;
    }
    else {
      Serial.printf("Failed attempt to init CAN\t%d\n", i);
      delay(100);
    }
  }

  //Check if uSD card initialized
  if (!SD.begin(SPI_uSD_PIN)) {
    Serial.println("uSD card failed to initialize, or is not present! No logging possible...");
  }
  /*
  else {
    Serial.println("uSD card initialized.");
    delay(1500);
    //Open uSD file to log data
    int counter = 0;
    do {
      char fileName[128];
      sprintf(fileName, "data_%03d.txt", counter++);
      dataFile = SD.open(fileName, FILE_WRITE);
      if (counter > 10) {
        Serial.printf("Error opening '%s'! Logging disabled!", fileName);
      }
    } while (!dataFile);
  }
  */
}


void sendLineToFIS(INT8U *buf, INT32U canId = FIS_UPPER_CAN_ID, INT8U msgLen = 8) {
  for (int i = 0; i < 5; ++i) {
    CAN.sendMsgBuf(canId, 0, 0, msgLen, buf);
    delay(100);
  }
}

void queryForFIN() {
  unsigned long startMillis = millis();
  Serial.printf("Start queryForFIN at %d ms\n", startMillis);
  // 1. set Filter/Mask
  // all 11 bits of standard id must match ...
  CAN.init_Mask(0, 0, 0x7FF);
  CAN.init_Mask(0, 0, 0x000);
  // CAN.init_Mask(1, 0, 0x00);
  // ... the FIN_CAN_ID bits
  CAN.init_Filt(0, 0, FIN_CAN_ID);
  CAN.init_Filt(1, 0, FIN_CAN_ID_2);
  // 2. send RTR Message
  unsigned char msgLen = 1;
  delay(200);
  char sendMsg[1] = { 0xE0 };
  if (CAN_OK == CAN.sendMsgBuf(FIN_CAN_ID_2, 1, 0, msgLen, NULL)) {
    Serial.printf("RTR Msg sent to %03X\n", FIN_CAN_ID_2);
    // 3. wait for reply
    unsigned long start = 50;
    while (start-- > 0) {
      if (Serial.available()) {
        break;
      }
      INT8U msgBuf[256];
      Serial.println("Waiting...");
      if (CAN_MSGAVAIL == CAN.checkReceive()) {
        CAN.readMsgBuf(&msgLen, msgBuf);
        Serial.printf("%10ld:\tID[%03X]\t", micros(), CAN.getCanId());
        if (CAN.getRtrFlag()) {
          Serial.printf("R\t");
        }
        Serial.printf("Message[%d]:\t", msgLen);
        for (int j=0; j<msgLen; j++) {
          Serial.printf(" %02X", msgBuf[j]);
        }
        Serial.println();

        // 5. show line in FIS 1st line
        // sendLineToFIS(FIS_UPPER_CAN_ID, 4, upper);
        // 6. show time in FIS 2nd line
        // sendLineToFIS(FIS_LOWER_CAN_ID, 4, lower);
      }
    }
  }
  CAN.init_Mask(0, 0, 0x00);
  // CAN.init_Mask(1, 0, 0x00);
  // ... the DATE_TIME_CAN_ID bits
  CAN.init_Filt(0, 0, DATE_TIME_CAN_ID);
  CAN.init_Filt(1, 0, DATE_TIME_CAN_ID_2);
  Serial.printf("End of queryForFIN at %d ms! Runtime ~ %d seconds.\n", millis(), (startMillis/millis())/1000);
}

void queryForDate() {
  Serial.printf("Start queryForDate at %d\n", micros());
  // 1. set Filter/Mask
  // all 11 bits of standard id must match ...
  CAN.init_Mask(0, 0, 0x7FF);
  CAN.init_Mask(0, 0, 0x7FF);
  // CAN.init_Mask(1, 0, 0x00);
  // ... the DATE_TIME_CAN_ID bits
  CAN.init_Filt(0, 0, DATE_TIME_CAN_ID);
  CAN.init_Filt(1, 0, DATE_TIME_CAN_ID_2);
  // 2. create RTR Message
  unsigned char msgLen = 0;
  if (CAN_OK == CAN.sendMsgBuf(DATE_TIME_CAN_ID, 0xFF, 0x00, msgLen, NULL)) {
    Serial.printf("RTR Msg sent to: %03X\n", DATE_TIME_CAN_ID);
    // 3. wait for reply
    unsigned long start = 50;
    while ((start--) > 0) {
      if (Serial.available()) {
        break;
      }
      msgLen = -1;
      INT8U msgBuf[256];
      Serial.println("Waiting...");
      if (CAN_MSGAVAIL == CAN.checkReceive()) {
        CAN.readMsgBuf(&msgLen, msgBuf);
        Serial.printf("%10ld:\tID[%03X]\t", micros(), CAN.getCanId());
        if (CAN.getRtrFlag()) {
          Serial.printf("R\t");
        }
        Serial.printf("MessageLength %d\n", msgLen);
        for (int c=0; c < msgLen; c++) {
          Serial.printf(" %02X", msgBuf[c]);
        }
        Serial.println();
        INT8U upper[4], lower[4];
        for (int i = 0; i < msgLen; i++) {
          // 4. print Date to Serial
          if (i < 4) {
            // TIME
            lower[i] = msgBuf[i];
          }
          else {
            // DATE
            upper[i-4] = msgBuf[i];
          }
        }

        // print date/time to Serial
        char date[8], timeBuf[8];
        for (int i = 0; i < 4; i++) {
          sprintf(date, "%02X-", (unsigned char) msgBuf[i]);
          sprintf(timeBuf, "%02X-", (unsigned char) msgBuf[i+4]);
        }
        Serial.printf("DateTime output as HEX: %s\t%s\n", date, timeBuf);

        /*// 5. show date in FIS 1st line
        sendLineToFIS(FIS_UPPER_CAN_ID, 4, upper);
        // 6. show time in FIS 2nd line
        sendLineToFIS(FIS_LOWER_CAN_ID, 4, lower); */
      }
    }
  }
  CAN.init_Mask(0, 0, 0x000);
  // CAN.init_Mask(1, 0, 0x00);
  // ... the DATE_TIME_CAN_ID bits
  CAN.init_Filt(0, 0, 0x00);
  CAN.init_Filt(1, 0, 0x00);
  Serial.printf("End queryForDate at %d", micros());
}

void sendInitToFis() {
  INT8U *ptr_msgBuf = (INT8U*) calloc(8, sizeof(INT8U));
  int msgLen = 8;
  // TODO
  INT8U line1[8] = { 'C', 'A', 'N', ' ', 'I', 'N', 'I', 'T' },
        line2[8] = { 'S', 'U', 'C', 'C', 'E', 'S', 'S', '!' };
  sendLineToFIS(line1);
  sendLineToFIS(line2, FIS_LOWER_CAN_ID);
}

void initTvModule() {
  unsigned char *ptr_msgBuf = (INT8U*) calloc(8, sizeof(unsigned char));
  // TODO
  // 1. init mask 0 to match all 11 std id bits
  CAN.init_Mask(0, 0, 0x7F8);
  CAN.init_Mask(1, 0, 0x7F8);
  // 2. init filter 0 & 1 for tv an nav modules
  CAN.init_Filt(0, 0, 0x3C0);
  CAN.init_Filt(1, 0, 0x3C1);
  CAN.init_Filt(2, 0, 0x661);
  CAN.init_Filt(3, 0, 0x65F);
  CAN.init_Filt(4, 0, 0x623);
  CAN.init_Filt(5, 0, 0x4C8);

  unsigned char msgLen;
  while (true) {
    byte msgBuf[256];
    if (Serial.available()) {
      break;
    }
    if (CAN_MSGAVAIL == CAN.checkReceive()) {
      CAN.readMsgBuf(&msgLen, msgBuf);
      Serial.printf("%10ld:\tID[%03X]\t", micros(), CAN.getCanId());
      if (CAN.getRtrFlag()) {
        Serial.printf("R\t");
      }
      Serial.printf("Message[%d]:\t", msgLen);
      for (int j=0; j<msgLen; j++) {
        Serial.printf(" %02X", msgBuf[j]);
      }
      Serial.println();
      CAN.clearMsg();
      // 5. show line in FIS 1st line
      // sendLineToFIS(FIS_UPPER_CAN_ID, 4, upper);
      // 6. show time in FIS 2nd line
      // sendLineToFIS(FIS_LOWER_CAN_ID, 4, lower);
    }
  }
  // 3. start init
  unsigned char *ptr_reqMsg;
  int j = 0;
  for (ptr_reqMsg = TV_INIT_MSGS[0]; *ptr_reqMsg; ptr_reqMsg++) {
    ptr_msgBuf[j++] = *ptr_reqMsg;
    if (*ptr_reqMsg == '\0') {
      ptr_msgBuf[j] = '\0';
      break;
    }
  }
}

int run = 0;
char serialInput;
void loop() {
  switch (run) {
    case 1:
      Serial.println("try queryForDate");
      queryForDate();
      break;
    case 2:
      Serial.println("try queryForFIN");
      queryForFIN();
      break;
    case 3:
      Serial.println("try tvModulInit");
      initTvModule();
      break;
  }
  run = 0;

  if (Serial.available()) {
    serialInput = Serial.read();
    Serial.printf("got a %c\n", serialInput);
    switch (serialInput) {
      case 'r':
        run = 1;
        break;
      case 'f':
        run = 2;
        break;
      case 't':
        run = 3;
        break;
    }
    // break;
  }
}
